
import React from 'react';
import {Text} from 'react-native';
import { Button} from 'native-base';
export default class SignIn extends React.Component {

  render() {
    return (
        <Button rounded success style={{alignSelf:'center',flex:1,flexDirection:'row',width:150}}>
        <Text style={{color:'white', fontSize:25}} >Sign in</Text>
        </Button>
    );
  }
}

 