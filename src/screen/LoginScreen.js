import React from 'react'
import {View,Image, Text,TouchableOpacity, StyleSheet} from 'react-native' 
import DrawerButton from '../component/DrawerButton';
import { Container, Header, Content, Form, Item, Input, Label,Icon } from 'native-base';
import SignIn from './../component/ButtonSign';
export default class HomeScreen extends React.Component {
  render(){
    return(     
       <Container >          
       <DrawerButton navigation={this.props.navigation}/>
        <Header style={{backgroundColor:'green',height:300,alignItems:'center',marginBottom:40}}>
        <Image source={require('./../../assets/londry.png')} style={{width:150,height:150}} />
        </Header>
        
        <Content>
          <Form style={{marginBottom:25,marginLeft:25,marginRight:25}}>
            <Item inlineLabel last success>
              <Label>Username</Label>
              <Input />
            </Item>
            <Item inlineLabel last success>
              <Label>Password</Label>
              <Input />
            </Item >       
            </Form>
            <Form style={{alignItems:'center'}}>
            <SignIn/>
            <TouchableOpacity>
            <Text style={{fontSize:20,marginTop:20,color:'grey'}}>
                Forgot Password?
            </Text>
            </TouchableOpacity>
            <Item inlineLabel last success style={{width:300,marginTop:55}}/>
            <TouchableOpacity>
            <Text style={{fontSize:20,marginTop:20,color:'grey'}}>
                New here? Sign up
            </Text>
            </TouchableOpacity>
            </Form>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  container: {  
    top: 300,
    alignItems: 'center',
    justifyContent: 'center',
  }
})